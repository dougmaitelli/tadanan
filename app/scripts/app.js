angular.module("tadanan", ['ui.router']).config([
  '$httpProvider',
  '$stateProvider',
  '$urlRouterProvider',
  function($httpProvider, $stateProvider, $urlRouterProvider) {

		$stateProvider.state('home', {
			url: "/",
      views: {
        '': {
          templateUrl : 'views/home.html',
		      controller : 'HomeController'
        }
      }
	  }).state('insertForm', {
      url: "/insert",
      views: {
        '': {
          templateUrl : 'views/insertForm.html',
          controller : 'InsertFormController'
        },
        'companyData@insertForm' : {
          templateUrl : 'views/form/companyData.html'
        },
        'ownerData@insertForm' : {
          templateUrl : 'views/form/ownerData.html'
        },
        'businessExperience@insertForm' : {
          templateUrl : 'views/form/businessExperience.html'
        },
        'financeData@insertForm' : {
          templateUrl : 'views/form/financeData.html'
        },
        'marketData@insertForm' : {
          templateUrl : 'views/form/marketData.html'
        },
        'planningData@insertForm' : {
          templateUrl : 'views/form/planningData.html'
        },
        'organizationData@insertForm' : {
          templateUrl : 'views/form/organizationData.html'
        },
        'peopleData@insertForm' : {
          templateUrl : 'views/form/peopleData.html'
        }
      }
    }).state('editForm', {
      url: "/edit/:id",
      views: {
        '': {
          templateUrl : 'app/views/insertForm.html',
          controller : 'InsertFormController'
        },
        'companyData@editForm' : {
          templateUrl : 'views/form/companyData.html'
        },
        'ownerData@editForm' : {
          templateUrl : 'views/form/ownerData.html'
        },
        'businessExperience@editForm' : {
          templateUrl : 'views/form/businessExperience.html'
        },
        'financeData@editForm' : {
          templateUrl : 'views/form/financeData.html'
        },
        'marketData@editForm' : {
          templateUrl : 'views/form/marketData.html'
        },
        'planningData@editForm' : {
          templateUrl : 'views/form/planningData.html'
        },
        'organizationData@editForm' : {
          templateUrl : 'views/form/organizationData.html'
        },
        'peopleData@editForm' : {
          templateUrl : 'views/form/peopleData.html'
        }
      }
    }).state('search', {
      url: "/search",
      views: {
        '': {
          templateUrl : 'views/search.html',
          controller : 'SearchController'
        }
      }
    }).state('exportImport', {
      url: "/exportImport",
      views: {
        '': {
          templateUrl : 'views/exportImport.html',
          controller : 'ExportImportController'
        }
      }
    }).state('reports', {
      url: "/reports",
      views: {
        '': {
          templateUrl : 'views/reports.html',
          controller : 'ReportsController'
        }
      }
    });

		$urlRouterProvider.otherwise('/');

  }
]);

angular.module('tadanan').directive('capitalize', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, modelCtrl) {
      var capitalize = function(inputValue) {
        if (inputValue == undefined) inputValue = '';
        var capitalized = inputValue.toUpperCase();
        if (capitalized !== inputValue) {
          modelCtrl.$setViewValue(capitalized);
          modelCtrl.$render();
        }
        return capitalized;
      }
      modelCtrl.$parsers.push(capitalize);
      capitalize(scope[attrs.ngModel]); // capitalize initial value
    }
  };
});
