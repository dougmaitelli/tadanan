angular.module('tadanan').factory('MunicipalitiesService', function() {

	var oService = {
    cities: null
  };

  oService.getMunicipalitiesPath = function() {
    return 'assets/rs-municipalities.json';
  };

	oService.getCities = function() {
    if (!this.cities) {
      var cities = [];

      var data = $.ajax({
        url: this.getMunicipalitiesPath(),
        async: false,
        dataType: 'json'
      });

    	data.responseJSON.objects.municipalities.geometries.forEach(function(item) {
    	  cities.push({key: item.id, name: item.properties.name});
    	});

      this.cities = cities;
    }

    return this.cities;
	};

	return oService;
});
