angular.module('tadanan').controller('ExportImportController', ['$scope', 'DbService', function($scope, DbService) {

	var fs = require('fs');

	function exportData(exportPath) {
		var writeStream = fs.createWriteStream(exportPath);

		var collection = DbService.getQuestionnaireCollection();

		collection.find({}).toArray(function(err, items) {
	      if (err) throw err;

		  writeStream.write(JSON.stringify(items));
		  writeStream.end();

		  swal("Sucesso!", "Registros exportados com sucesso.", "success");
	    });
	}

	$scope.exportDialog = function() {
		const {dialog} = require('electron').remote;

		var exportPath = dialog.showSaveDialog({
		  filters: [
		    {name: 'JSON', extensions: ['json']}
		  ]
		});
		exportData(exportPath);
	};

	function importData(importPath) {
		var collection = DbService.getQuestionnaireCollection();

		fs.readFile(importPath, 'utf8', function(err, data) {
		  if (err) throw err;

		  var importEntries = JSON.parse(data);

		  importEntries.forEach(function(item) {
		  	delete item._id;

		  	collection.insert(item);
		  });

		  swal("Sucesso!", "Registros importados com sucesso.", "success");
		});
	}

	$scope.importDialog = function() {
		const {dialog} = require('electron').remote;

		var importPath = dialog.showOpenDialog({
		  properties: ['openFile'],
		  filters: [
		    {name: 'JSON', extensions: ['json']}
		  ]
		});
		importData(importPath[0]);
	};

}]);
