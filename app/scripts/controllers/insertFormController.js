angular.module('tadanan').controller('InsertFormController', ['$scope', '$state', '$stateParams', '$timeout', 'DbService', 'MunicipalitiesService', function($scope, $state, $stateParams, $timeout, DbService, MunicipalitiesService) {

  var collection = DbService.getQuestionnaireCollection();

  $scope.data = {};
  $scope.cities = MunicipalitiesService.getCities();

  $timeout(function() {
    $('.mask.cnpj').mask('00.000.000/0000-00', {reverse: true});
    $('.mask.date').mask('00/00/0000');

    $('#form').form({
      onSuccess: function(evt) {
        $scope.save();
        evt.preventDefault();
      },
      fields: {
        cnpj: {
          identifier: 'cnpj',
          rules: [
            {
              type   : 'empty',
              prompt : 'Favor informar CNPJ'
            }
          ]
        },
        name: {
          identifier: 'name',
          rules: [
            {
              type   : 'empty',
              prompt : 'Favor informar Razão Social'
            }
          ]
        }
      }
    });

    if ($stateParams.id) {
      $(".ui.loadingIndicator").addClass("active");

      collection.findOne({
        _id: $stateParams.id
      }, function(err, result) {
        $timeout(function() {
          $scope.data = result;

          $timeout(function() {
            $('.ui.dropdown').dropdown();
            $(".ui.loadingIndicator").removeClass("active");
          }, 100);
        }, 0);
      });
    } else {
      $('.ui.dropdown').dropdown();
    }
  }, 0);

  $scope.calculateAge = function(birthdayStr) {
    if (!birthdayStr || birthdayStr.length < 10) {
      return 0;
    }

    birthdayStr = birthdayStr.split("/");
    var birthday = new Date(birthdayStr[2] + '-' + birthdayStr[1] + '-' + birthdayStr[0]);
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  $scope.save = function() {
    if (!$('#form').form('is valid')) {
      return;
    }

    var successCallback = function(err, result) {
      $timeout(function() {
        $scope.data._id = $scope.data._id;
      }, 0);

      swal({
        title: "Sucesso!",
        text: "Registro salvo com sucesso!",
        type: "success"
      }, function() {
        $state.go("search");
      });
    };

    if (!$scope.data._id) {
      collection.insert($scope.data, successCallback);
    } else {
      collection.update({_id: $scope.data._id}, $scope.data, successCallback);
    }
  };

  $scope.delete = function() {
    swal({
      title: "Você tem certeza?",
      text: "Esta ação não poderá ser desfeita!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Sim, deletar!",
      closeOnConfirm: false
    }, function() {
      collection.remove({_id: $scope.data._id}, function(err) {
        swal({
          title: "Deletado!",
          text: "O registro foi deletado.",
          type: "success"
        }, function() {
          $state.go("search");
        });
      });
    });
  };

}]);
