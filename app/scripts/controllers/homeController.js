angular.module('tadanan').controller('HomeController', ['$scope', '$timeout', 'DbService', 'MunicipalitiesService', function($scope, $timeout, DbService, MunicipalitiesService) {

  var waterAreas = ["LAGOA DOS PATOS"];

  function aggregateData(items) {
    var aggregated = {};

    items.forEach((item) => {
      var aggregateValue = item.company.city;

      if (!aggregateValue) {
        return;
      }

      if (!aggregated[aggregateValue]) {
        aggregated[aggregateValue] = {
          quantity: 0
        };
      }

      aggregated[aggregateValue].quantity++;
    });

    return aggregated;
  }

  function plotMap(data) {
    var onlyValues = [];

    for (var i in data) {
      if (data.hasOwnProperty(i)) {
        onlyValues.push(data[i].quantity);
      }
    }

    var minValue = Math.min.apply(null, onlyValues);
    var maxValue = Math.max.apply(null, onlyValues);

    var paletteScale = d3.scale.linear()
      .domain([minValue, maxValue])
      .range(["#EFEFFF", "#02386F"]); // blue color

    for (var i in data) {
      if (data.hasOwnProperty(i)) {
        data[i].fillColor = paletteScale(data[i].quantity);
      }
    }

    waterAreas.forEach(function(area) {
      data[area] = {fillColor: '#A6C4F2'};
    });

    var map = new Datamap({
      element: $('#mapContainer')[0],
      responsive: true,
      scope: 'municipalities',
      geographyConfig: {
        dataUrl: MunicipalitiesService.getMunicipalitiesPath(),
        borderColor: '#B7B7B7',
        highlightFillColor: function(geo) {
          return geo['fillColor'] || '#F5F5F5';
        },
        highlightBorderColor: '#B7B7B7',
        highlightBorderWidth: 2,
        popupTemplate: function(geography, data) {
          return '<div class="hoverinfo">' + geography.properties.name + (data && data.quantity ? '(' +  data.quantity + ')' : '') + '</div>';
        }
      },
      setProjection: function(element, options) {
        var projection, path;
          projection = d3.geo.equirectangular()
            .center([-51.1539876, -29.6907222])
            .scale(element.offsetWidth * 15)
            .translate([element.offsetWidth / 2, element.offsetHeight / 2]);
          path = d3.geo.path()
            .projection(projection);

        return {path: path, projection: projection};
      },
      fills: {defaultFill: '#D2E4C8'},
      data: data,
      done: function(datamap) {
        datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography, data) {
          swal("Teste", console.log(geography), "success");
        });
      }
    });

    return map;
  }

  $timeout(function() {
    var collection = DbService.getQuestionnaireCollection();

    collection.find().toArray(function(err, items) {
      if (err) throw err;

      var data = aggregateData(items);

      var map = plotMap(data);

      $(window).on('resize', function() {
        map.resize();
      });
    });
  }, 0);

}]);
