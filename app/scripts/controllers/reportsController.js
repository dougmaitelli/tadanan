angular.module('tadanan').controller('ReportsController', ['$scope', '$timeout', 'DbService', function($scope, $timeout, DbService) {

  $scope.keyField = null;
  $scope.valueFields = [];

  $scope.keyFieldOptions = [
    {fieldName: "company.segment", type: "enum"}
  ];
  $scope.valueFieldOptions = ["company.numberOfEmployees"];

  $timeout(function() {
    $('.ui.dropdown').dropdown();
  }, 0);

  $scope.toggleValueFieldSelection = function(value) {
    var idx = $scope.valueFields.indexOf(value);

    if (idx > -1) {
      $scope.valueFields.splice(idx, 1);
    } else {
      $scope.valueFields.push(value);
    }
  };

  function aggregateData(items) {
    var aggregated = {};

    items.forEach((item) => {
      var aggregateValue = deepFind(item, $scope.keyField.fieldName);

      if (!aggregated[aggregateValue]) {
        aggregated[aggregateValue] = {};
      }

      $scope.valueFields.forEach((valueField) => {
        var valueFieldValue = deepFind(item, valueField);

        if (!aggregated[aggregateValue][valueField]) {
          aggregated[aggregateValue][valueField] = {
            totalSum: 0,
            count: 0
          };
        }

        if (!valueFieldValue) {
          return;
        }

        aggregated[aggregateValue][valueField].totalSum += parseInt(valueFieldValue);
        aggregated[aggregateValue][valueField].count++;
      });
    });

    for (var i in aggregated) {
      if (aggregated.hasOwnProperty(i)) {
        for (var x in aggregated[i]) {
          if (aggregated[i].hasOwnProperty(x)) {
            aggregated[i][x].average = aggregated[i][x].totalSum / aggregated[i][x].count;
          }
        }
      }
    }

    return aggregated;
  }

  function deepFind(obj, path) {
    var paths = path.split('.')
    var current = obj

    for (var i = 0; i < paths.length; ++i) {
      if (current[paths[i]] == undefined) {
        return undefined;
      } else {
        current = current[paths[i]];
      }
    }

    return current;
  }

  function extractKeyFieldValues(aggregated) {
    var values = [];

    for (var i in aggregated) {
      if (aggregated.hasOwnProperty(i)) {
        values.push(i);
      }
    }

    return values;
  }

  function extractSeries(aggregated) {
    var values = [];

    for (var i in aggregated) {
      if (aggregated.hasOwnProperty(i)) {

        var seriesCount = 0;
        for (var x in aggregated[i]) {
          if (aggregated[i].hasOwnProperty(x)) {
            if (!values[seriesCount]) {
              values[seriesCount] = {label: $scope.valueFields[seriesCount], data: []};
            }

            values[seriesCount].data.push(aggregated[i][x].average);

            seriesCount++;
          }
        }
      }
    }

    return values;
  }

  $scope.plotChart = function() {
    var collection = DbService.getQuestionnaireCollection();

    collection.find().toArray(function(err, items) {
      if (err) throw err;

      var aggregated = aggregateData(items);

      var labels = extractKeyFieldValues(aggregated);
      var series = extractSeries(aggregated);

      var ctx = $("#reportChart")[0];

      var data = {
        labels: labels,
        datasets: series
      };

      var options = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        maintainAspectRatio: false,
        responsive: true
      };

      var chart;
      if ($scope.keyField.type === "enum") {
        chart = new Chart(ctx, {
          type: 'bar',
          data: data,
          options: options
        });
      } else {
        chart = new Chart(ctx, {
          type: 'line',
          data: data,
          options: options
        });
      }
    });
  };

}]);
